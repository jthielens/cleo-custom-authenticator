package com.cleo.labs.auth;

import java.util.ArrayList;
import java.util.List;

import com.cleo.lexicom.LexiCom;
import com.cleo.lexicom.VLProxyData;
import com.cleo.lexicom.beans.FtpUserHost;
import com.cleo.lexicom.beans.FtpUserMailbox;
import com.cleo.lexicom.beans.LocalUserHost;
import com.cleo.lexicom.beans.SshFtpUserHost;
import com.cleo.lexicom.beans.SshFtpUserMailbox;
import com.cleo.lexicom.beans.httpserver.HttpUserHost;
import com.cleo.lexicom.beans.httpserver.HttpUserMailbox;
import com.cleo.lexicom.external.IExtendedUserGroups;
import com.cleo.lexicom.vlnavigator.ldap.IUserDirectoryService;
import com.cleo.lexicom.vlnavigator.ldap.LdapUser;

/**
 * In system.properties set:
 *     cleo.extended.usergroups=<class extending com.cleo.labs.auth.CustomExternalAuthStub>
 *
 * The JAR should be copied to lib/api.
 */
public abstract class CustomExternalAuthStub implements IExtendedUserGroups {

    public enum Protocol {FTP, SFTP, HTTP};

    /*========================================================================*\
     * Custom methods.  Implement or override these and magic occurs.         *
    \*========================================================================*/
    /**
     * Abstract method to return the mailbox alias.  This value is not the user
     * name, but is like the alias of an LDAP mailbox.  It will not appear in
     * the UI, but will appear in logs and transfer reports.
     * @return the alias
     */
    public abstract String getAlias();
    /**
     * Abstract method to return the mailbox root.  The actual home directory
     * for the user will be created by appending the username to this value.
     * @return the mailbox root
     */
    public abstract String getRoot();
    /**
     * Default method to return the mailbox inbox, the subdirectory from the
     * user's mailbox root into which outgoing files (files going "in" to the
     * VersaLex router) should be placed.  "outbox" by default.
     * @return the inbox
     */
    public String getInbox() {
        return "outbox";
    }
    /**
     * Default method to return the mailbox outbox, the subdirectory from the
     * user's mailbox root into which incoming files (files being sent "out"
     * from the VersaLex router to the user) are placed.  "inbox" by default.
     * @return the outbox
     */
    public String getOutbox() {
        return "inbox";
    }
    /**
     * Abstract method to authenticate a user.
     * @param protocol the protocol the user is attempting to use for login
     * @param username the user's username
     * @param password the user's password
     * @return {@code true} if the user should be logged in
     */
    public abstract boolean authenticate(Protocol protocol, String username, String password);

    /**
     * Default method to check for a user's existence.  By default, returns
     * {@code true} for any user, which is fine if the system is using the
     * {@link IExtendedUserGroups} exclusively.  But if used in conjunction
     * with LDAP, this method should return {@code false} if the user is
     * not (likely to be) a valid External user, or conversely, if the
     * user is (likely to be) an LDAP user.
     * @param username
     * @return {@code true} if the user is (likely to be) a valid user
     */
    public boolean contains(String username) {
        return true;
    }

    /**
     * Reusable empty list for {@link #getUsers()}.
     */
    private static final ArrayList<LdapUser> NO_USERS = new ArrayList<>(0);

    /**
     * Used for synchronization, which is not necessary for External users.
     * @return an empty list
     */
    public List<LdapUser> getUsers() {
        return NO_USERS;
    }

    /**
     * Sets the basic minimal required properties for the synthetic "magic"
     * local user host on the indicated protocol.  This will be called once
     * for each protocol.  Override this method to set additional properties
     * or to handle the assignment differently.
     * @param protocol the protocol
     * @param host the {@link LocalUserHost} appropriate for {@code protocol}
     */
    public void setMailboxProperties(Protocol protocol, LocalUserHost host) {
        host.setAlias(getAlias()+" "+protocol.name());
        host.setFtpRootPath(getRoot());
        host.setInbox(getInbox());
        host.setOutbox(getOutbox());
    }

    /*========================================================================*\
     * FTP wrapper.  Delegates to static methods.                             *
    \*========================================================================*/
    public static class CustomFtpHost extends FtpUserHost {
        private static final long serialVersionUID = -4778510186030128874L;

        public CustomFtpHost(CustomExternalAuthStub auth) {
            super();
            auth.setMailboxProperties(Protocol.FTP, this);

            // create a mailbox that will hold all of the magic users
            CustomFtpMailbox mailbox = new CustomFtpMailbox(auth);
            mailbox.beanInitialized = true;
            setMailbox(0, mailbox);
        }
        @Override
        public boolean isReady() throws Exception {
            return (LexiCom.activeXMLproxy.getLocalHost().isFtpReady() ||
                    LexiCom.activeXMLproxy.getLocalHost().isFtpsAuthReady() ||
                    LexiCom.activeXMLproxy.getLocalHost().isFtpsImplReady() ||
                    VLProxyData.isRevProxyFtp());
        }

    }
    public static class CustomFtpMailbox extends FtpUserMailbox implements IUserDirectoryService {
        private static final long serialVersionUID = 2810047620869601482L;
        private CustomExternalAuthStub auth;

        public CustomFtpMailbox(CustomExternalAuthStub auth) {
            super();
            this.auth = auth;
            setAlias(auth.getAlias()+" Users");
            setLdapUserGroup(true);
        }

        @Override
        public boolean isReady() throws Exception {
            return getParentBean().isReady();
        }

        @Override
        public boolean authenticate(String username, String password, boolean throwAuthException)
                throws Exception {
            return auth.authenticate(Protocol.FTP, username, password);
        }

        @Override
        public boolean contains(String username) {
            return auth.contains(username);
        }

        @Override
        public List<LdapUser> getUsers() throws Exception {
            return auth.getUsers();
        }
        
    }

    /*========================================================================*\
     * SFTP wrapper.  Delegates to static methods.                            *
    \*========================================================================*/
    public static class CustomSftpHost extends SshFtpUserHost {
        private static final long serialVersionUID = 7546987938846542580L;
        private CustomSftpMailbox mailbox;

        public CustomSftpHost(CustomExternalAuthStub auth) {
            super();
            auth.setMailboxProperties(Protocol.SFTP, this);

            // create a mailbox that will hold all of the magic users
            mailbox = new CustomSftpMailbox(auth);
            mailbox.beanInitialized = true;
            setMailbox(0, mailbox);
        }
        @Override
        public boolean isReady() throws Exception {
            return (LexiCom.activeXMLproxy.getLocalHost().isSshFtpReady() ||
                    VLProxyData.isRevProxySshFtp());
         
        }
        @Override
        public SshFtpUserMailbox getSshFtpUserMailbox(String mailbox) {
            return this.mailbox;
        }

    }
    public static class CustomSftpMailbox extends SshFtpUserMailbox implements IUserDirectoryService {
        private static final long serialVersionUID = -8594163422765668753L;
        private CustomExternalAuthStub auth;

        public CustomSftpMailbox(CustomExternalAuthStub auth) {
            super();
            this.auth = auth;
            setAlias(auth.getAlias()+" Users");
            setLdapUserGroup(true);
        }

        @Override
        public boolean isReady() throws Exception {
            return getParentBean().isReady();
        }

        @Override
        public boolean authenticate(String username, String password, boolean throwAuthException)
                throws Exception {
            return auth.authenticate(Protocol.SFTP, username, password);
        }

        @Override
        public boolean contains(String username) {
            return auth.contains(username);
        }

        @Override
        public List<LdapUser> getUsers() throws Exception {
            return auth.getUsers();
        }
        
    }

    /*========================================================================*\
     * HTTP wrapper.  Delegates to static methods.                            *
    \*========================================================================*/
    public static class CustomHttpHost extends HttpUserHost {
        private static final long serialVersionUID = 3128875667066503717L;

        public CustomHttpHost(CustomExternalAuthStub auth) {
            super();
            auth.setMailboxProperties(Protocol.HTTP, this);

            // create a mailbox that will hold all of the magic users
            CustomHttpMailbox mailbox = new CustomHttpMailbox(auth);
            mailbox.beanInitialized = true;
            setMailbox(0, mailbox);
        }
        @Override
        public boolean isReady() throws Exception {
            return LexiCom.activeXMLproxy.getLocalHost().isHttpReady() ||
                   LexiCom.activeXMLproxy.getLocalHost().isHttpsReady();
         
        }

    }
    public static class CustomHttpMailbox extends HttpUserMailbox implements IUserDirectoryService {
        private static final long serialVersionUID = -700780748880706389L;
        private CustomExternalAuthStub auth;

        public CustomHttpMailbox(CustomExternalAuthStub auth) {
            super();
            this.auth = auth;
            setAlias(auth.getAlias()+" Users");
            setLdapUserGroup(true);
        }

        @Override
        public boolean isReady() throws Exception {
            return getParentBean().isReady();
        }

        @Override
        public boolean authenticate(String username, String password, boolean throwAuthException)
                throws Exception {
            return auth.authenticate(Protocol.HTTP, username, password);
        }

        @Override
        public boolean contains(String username) {
            return auth.contains(username);
        }

        @Override
        public List<LdapUser> getUsers() throws Exception {
            return auth.getUsers();
        }

        /**
         * This method is needed in the {@link CustomHttpMailbox} to accommodate
         * VLPortal, which otherwise thinks an ExtendedUserGroup is truly an LDAP.
         * This implementation returns its argument unchanged.
         */
        @Override
        public String getMixedCaseUsername(String username) {
            return username;
        }        
    }

    /*========================================================================*\
     * Outermost wrapper                                                      *
    \*========================================================================*/
    private CustomFtpHost  customFtpHost;
    private CustomSftpHost customSftpHost;
    private CustomHttpHost customHttpHost;

    public CustomExternalAuthStub() {
        customFtpHost  = new CustomFtpHost(this);
        customSftpHost = new CustomSftpHost(this);
        customHttpHost = new CustomHttpHost(this);
    }

    @Override
    public LocalUserHost[] getFtpExtendedUserGroups() {
        return new LocalUserHost[] {customFtpHost};
    }

    @Override
    public LocalUserHost[] getSshFtpExtendedUserGroups() {
        return new LocalUserHost[] {customSftpHost};
    }

    @Override
    public LocalUserHost[] getHttpExtendedUserGroups() {
        return new LocalUserHost[] {customHttpHost};
    }

}
