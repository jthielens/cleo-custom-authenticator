package com.cleo.labs.auth;

public class AnyJoe extends CustomExternalAuthStub {

    private static final String ALIAS  = "Any Joe";
    private static final String ROOT   = "local/root";
    private static final String INBOX  = "./";
    private static final String OUTBOX = "./";

    public AnyJoe() {
        super();
    }

    @Override
    public String getAlias() {
        return ALIAS;
    }

    @Override
    public String getRoot() {
        return ROOT;
    }

    @Override
    public String getInbox() {
        return INBOX;
    }

    @Override
    public String getOutbox() {
        return OUTBOX;
    }

    @Override
    public boolean authenticate(Protocol protocol, String username, String password) {
        return password!=null && !password.isEmpty() &&
               username.contains("joe");
    }

    @Override
    public boolean contains(String username) {
        return username.contains("joe");
    }

}
