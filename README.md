# README #

## Custom External Authentication ##

VersaLex supports three types of authentication for Local Users (users logging in to browse, upload, and download files over the FTP, SSH, and HTTP protocols):

1. **Locally defined users**, each defined as a *Mailbox* in a *Local User Host*.  The layout of the user's mailbox (folder structure) is defined int the Host.  The mailbox *Alias* is the username, and the credentials are defined as additional mailbox attributes.
2. **Synchronized users**, defined by way of LDAP filters.  Like locally defined users, the mailbox structure is defined in a *Local User Host*, while the *Mailbox* contains the LDAP filter string (query).  VersaLex periodically synchronizes its internal user list with LDAP, but these mailboxes do not appear in the admin UI.  Authentication is delegated back to the LDAP directory using a `bind` operation&mdash;passwords are not stored for LDAP users.
3. **Delegated users**, defined through an implementation of the extension interface `IExtendedUserGroups`.  Delegated users are treated internally in a way similar to LDAP users, but the Hosts and Mailboxes are never displayed in the Admin UI and are completely under the control of the API implementation.  No synchronization occurs, and each login attempt is passed through the API to the implementation's `authenticate` method.

The `CustomExternalAuthStub` abstract class provides an easy to use framework for implementing custom user delegation.

## Writing a Custom Authentication Plugin ##

The `IExtendedUserGroups` interface is modeled on the internal VersaLex LDAP mechanism.  Like that mechanism, a special *mailbox* represents a group of users, which are assigned to a specific *host*.  The *mailbox* and *host* in combination serve as a template from which concrete mailboxes are created when the user logs in.

Unlike LDAP, however, the *hosts* are automatically created by the `IExtendedUserGroups` plugin class, one for each supported protocol of `http`, `ssh`, and `ftp`, and each of these hosts provides a single fixed template *mailbox*.  These host and mailbox objects are created once during system startup by the `CustomExternalAuthStub` abstract base class.

### Create the mailbox template ###

It remains the job of the concrete implementation to provide the details of the mailbox template:

attribute | description | method | default
--------- | ----------- | ------ | -------
alias     | the alias | `getAlias()` | none
root      | the root storage of the user's mailbox, either a URI or a file reference | `getRoot()` | none
outbox    | the "pickup" subdirectory from the root from which the user is meant to pickup/download files | `getOutbox()` | `"inbox"`
inbox     | the "dropoff" subdirectory from the root into which the user is meant to drop off/upload files | `getInbox()` | `"outbox"`


Take special note that the traditional `inbox` and `outbox` roles are reversed.  What VersaLex calls the `inbox` or `outbox` is always from a server-centric perspective: the inbox is for "files going in to VersaLex" and the outbox is for "files going out from VersaLex", and this perspective is consistent between remote hosts and local users.  From an end-user's perspective, this would typically be reversed, and the default values in this implementation reflect the **end-user's perspective**.  Override the defaults if this is not what is intended.

These methods provide a minimal set of attributes for a mailbox.  To provide additional settings, override:

```
public void setMailboxProperties(Protocol protocol, LocalUserHost host) {
    host.setAlias(getAlias()+" "+protocol.name());
    host.setFtpRootPath(getRoot());
    host.setInbox(getInbox());
    host.setOutbox(getOutbox());
}
```

This method will be invoked three times, once for each `Protocol`, and with each invocation the `host` argument will be a specific subclass of `LocalUserHost` appropriate for the protocol.

A minimal implementation will override `getAlias()` and `getRoot()`.

### Process Logins ###

When a user attempts a login, VersaLex wil first attempt to match the user id to any explicitly defined mailbox.  If no match is found, VersaLex will then look for LDAP and `IExtendedUserGroups` mailboxes.  Since these mailboxes are really templates, VersaLex will inspect each for a match.  With LDAP, this macthing internal logic based on configured LDAP filters and a synchronization.

For `IExtendedUserGroups` the `public boolean contains(String username)` method is invoked to determine a match.  Once a unique matching mailbox is found, the password will later be passed to an `authenticate` method on the host object corresponding to the protocol.  In the `CustomExternalAuthStub`, the following two methods simplify this interface:

`public boolean contains(String username)`

Return `true` if `username` is valid.  This method is used typically after the user is logged in and the portal needs to rehydrate the mailbox.  It is ok for the method to return `true` for any `username` if this is known not to cause conflicts with possible LDAP users.  Otherwise this method must make an attempt to return `true` for valid plugin users, or at least `false` for LDAP users.

` public boolean authenticate(Protocol protocol, String username, String password)`

Return `true` to log `username` in on `protocol` with `password`.  There is no default implementation.

## Installation ##

### Manual ###

1. Place the JAR file in `lib/uri` and add it to the `cleo.additional.classpath` in `conf/system.properties`.  If the implementation requires additional jars, place these jars in `lib/uri` and add them to `cleo.additional.classpath` as well.
2. In `conf/system.properties` set `cleo.extended.usergroups` to the name of your custom authentication class.

### Cleo Labs Toolchain ###

If you are using the Cleo Labs Shell, set the `Cleo-API-ExtendedUserGroups` manifest property to the name of your custom class, add any required dependent JARs as `Cleo-URI-Additional` (and `Cleo-URI-Additional-n` as needed) properties using SBT *group*%*artifact*%*version* syntax.  Then install using:

```
shell.sh uri install /path/myjar.jar
```

You will need to restart the server after installation.